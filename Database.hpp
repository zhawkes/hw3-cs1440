/*
 * WARNING!
 *
 * You do not need to edit this file
 */

#pragma once

#include "Employment.hpp"
#include "Area.hpp"

/* declare data arrays so that they may be linked by other objects as needed */
extern Employment all_industries[];
extern Employment software_publishing[];
extern Area fips_areas[];


extern size_t size_all;
extern size_t size_soft;
extern size_t size_area;