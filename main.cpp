#include <iostream>

#include "Report.hpp"
#include "Database.hpp"
#include "Sort.hpp"
#include "Stats.hpp"
// include header files as needed


int main(void) {
	Report rpt;


	rpt.all.total_wages = total_annual_wages(all_industries);
	rpt.all.total_employment = total_employment_level(all_industries);
	rpt.all.per_capita_wage = per_capita_annual_wages(all_industries);
	
	rpt.soft.total_wages = total_annual_wages(software_publishing);
	rpt.soft.total_employment= total_employment_level(software_publishing);
	rpt.soft.per_capita_wage = per_capita_annual_wages(software_publishing);
	
	/**************************************************************************************
	 * All Industries Section
	 *************************************************************************************/
	// Top 5 Annual Wages
	sort_employment_by_total_annual_wages(all_industries, size_all / sizeof(Employment) - 1);
	for(int i = 0; i < TOP_N; ++i){
		rpt.all.top_annual_wages[i] = AreaTitleWithStat();
		rpt.all.top_annual_wages[i].area_title = area_fips_To_area_title(fips_areas, all_industries[i].area_fips);
		rpt.all.top_annual_wages[i].stat = all_industries[i].total_annual_wages;
	}
	
	//Bottom 5 annual Wages
	unsigned int size = (size_all / sizeof(Employment) - 2);
	for(unsigned int i = (size_all / sizeof(Employment) - 2); i > (size_all / sizeof(Employment) - 7); --i ){
		rpt.all.bot_annual_wages[size - i] = AreaTitleWithStat();
		rpt.all.bot_annual_wages[size - i].area_title = area_fips_To_area_title(fips_areas, all_industries[i].area_fips);
		rpt.all.bot_annual_wages[size - i].stat = all_industries[i].total_annual_wages;
 	}
	
	//Top 5 Employment Level
	sort_employment_by_annual_avg_emplvl(all_industries, size_all / sizeof(Employment) - 1);
	for(int i = 0; i < TOP_N; ++i){
		rpt.all.top_annual_avg_emplvl[i] = AreaTitleWithStat();
		rpt.all.top_annual_avg_emplvl[i].area_title = area_fips_To_area_title(fips_areas, all_industries[i].area_fips);
		rpt.all.top_annual_avg_emplvl[i].stat = all_industries[i].annual_avg_emplvl;
	}
	for(unsigned int i = (size_all / sizeof(Employment) - 2); i > (size_all / sizeof(Employment) - 7); --i ){
		rpt.all.bot_annual_avg_emplvl[size - i] = AreaTitleWithStat();
		rpt.all.bot_annual_avg_emplvl[size - i].area_title = area_fips_To_area_title(fips_areas, all_industries[i].area_fips);
		rpt.all.bot_annual_avg_emplvl[size - i].stat = all_industries[i].annual_avg_emplvl;
 	}
 	
 	/******************************************************************************
 	 *Software Publishing Section
 	 ******************************************************************************/
 	 
	sort_employment_by_total_annual_wages(software_publishing, size_soft / sizeof(Employment) - 1);
	for(int i = 0; i < TOP_N; ++i){
		rpt.soft.top_annual_wages[i] = AreaTitleWithStat();
		rpt.soft.top_annual_wages[i].area_title = area_fips_To_area_title(fips_areas, software_publishing[i].area_fips);
		rpt.soft.top_annual_wages[i].stat = software_publishing[i].total_annual_wages;
	}
	
	//Bottom 5 annual Wages
	size = (size_soft / sizeof(Employment) - 2);
	for(unsigned int i = (size_soft/ sizeof(Employment) - 2); i > (size_soft / sizeof(Employment) - 7); --i ){
		rpt.soft.bot_annual_wages[size - i] = AreaTitleWithStat();
		rpt.soft.bot_annual_wages[size - i].area_title = area_fips_To_area_title(fips_areas, software_publishing[i].area_fips);
		rpt.soft.bot_annual_wages[size - i].stat = software_publishing[i].total_annual_wages;
 	}
	
	//Top 5 Employment Level
	sort_employment_by_annual_avg_emplvl(software_publishing, size_soft / sizeof(Employment) - 1);
	for(int i = 0; i < TOP_N; ++i){
		rpt.soft.top_annual_avg_emplvl[i] = AreaTitleWithStat();
		rpt.soft.top_annual_avg_emplvl[i].area_title = area_fips_To_area_title(fips_areas, software_publishing[i].area_fips);
		rpt.soft.top_annual_avg_emplvl[i].stat = software_publishing[i].annual_avg_emplvl;
	}
	
	for(unsigned int i = (size_soft / sizeof(Employment) - 2); i > (size_soft / sizeof(Employment) - 7); --i ){
		rpt.soft.bot_annual_avg_emplvl[size - i] = AreaTitleWithStat();
		rpt.soft.bot_annual_avg_emplvl[size - i].area_title = area_fips_To_area_title(fips_areas, software_publishing[i].area_fips);
		rpt.soft.bot_annual_avg_emplvl[size - i].stat = software_publishing[i].annual_avg_emplvl;
 	}
	
	std::cout << rpt << std::endl;
}
